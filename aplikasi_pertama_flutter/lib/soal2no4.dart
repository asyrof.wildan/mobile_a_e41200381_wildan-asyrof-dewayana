import 'dart:io';
void main(List<String> args) {

  // //NO 1 
  // print("NO 1");
  // print("Apakah anda ingin menginstall aplikasi ini? Y/T");
  // var answer = stdin.readLineSync();
  // answer == "Y" ? print("Anda akan menginstall aplikasi dart"):
  // answer == "T" ? print("Aborted"):
  // print("Tidak sesuai!");
  // print("-------------------");

  // //NO 2 
  // print("N0 2");

  // print("Selamat datang,\nMasukkan nama anda : ");
  // var nama = stdin.readLineSync();
  // if (nama == null){
  //   print("Nama harus diisi!");
  // } else {
  //   print("Daftar peran:\nPenyihir | Guard | Werewolf\nMasukkan peran : ");
  //   var peran = stdin.readLineSync();

  //   if (peran == ""){
  //     print("Halo" + nama.toString() + ", Pilih Peranmu untuk memulai game!");
  //   } else if (peran == "Penyihir"){
  //     print("Selamat datang di Dunia Werewolf, "+ nama.toString() +".\nHalo Penyihir " + nama.toString() +", kamu dapat melihat siapa yang menjadi werewolf.");
  //   } else if (peran == "Guard"){
  //     print("Selamat datang di Dunia Werewolf, "+ nama.toString() +".\nHalo Guard " + nama.toString() +", kamu akan membantu melindungi temanmu dari serangan werewolf.");
  //   } else if (peran == "Werewolf"){
  //     print("Selamat datang di Dunia Werewolf, "+ nama.toString() +".\nHalo Werewolf " + nama.toString() +",  Kamu akan memakan mangsa setiap malam.");
  //   }
  // }

  // print("-------------------");

  // // NO 3
  // print("NO 3");

  // print("Masukkan hari :");
  // var hari = stdin.readLineSync();

  // switch(hari) {
  //   case "Senin": { print('Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja.'); break; }
  //   case "Selasa": { print('Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati.'); break; }
  //   case "Rabu": { print('Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri.'); break; }
  //   case "Kamis": { print('Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain'); break; }
  //   case "Jumat": { print('Hidup tak selamanya tentang pacar.'); break; }
  //   case "Sabtu": { print('Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan.'); break; }
  //   case "Minggu": { print('Hanya seseorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani.'); break; }
  //   default: { print('Masukkan hari yang benar!'); }
  // }
  // print("-------------------");

  //NO 4
  print("NO 4");
  var tanggal = 1;
  var bulan = 5;
  var tahun = 1945;

  switch(bulan) {
    case 1: { print(tanggal.toString() + ' Januari ' + tahun.toString()); break;}
    case 2: { print(tanggal.toString() + ' Februari ' + tahun.toString()); break;}
    case 3: { print(tanggal.toString() + ' Maret ' + tahun.toString()); break;}
    case 4: { print(tanggal.toString() + ' April ' + tahun.toString()); break;}
    case 5: { print(tanggal.toString() + ' Mei ' + tahun.toString()); break;}
    case 6: { print(tanggal.toString() + ' Juni ' + tahun.toString()); break;}
    case 7: { print(tanggal.toString() + ' Juli ' + tahun.toString()); break;}
    case 8: { print(tanggal.toString() + ' Agustus ' + tahun.toString()); break;}
    case 9: { print(tanggal.toString() + ' September ' + tahun.toString()); break;}
    case 10: { print(tanggal.toString() + ' Oktober ' + tahun.toString()); break;}
    case 11: { print(tanggal.toString() + ' November ' + tahun.toString()); break;}
    case 12: { print(tanggal.toString() + ' Desember ' + tahun.toString()); break;}
  }
  
}