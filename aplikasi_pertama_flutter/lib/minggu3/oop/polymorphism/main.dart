
import 'bangun_datar.dart';
import 'persegi.dart';
import 'segitiga.dart';
import 'lingkaran.dart';

void main(List<String> args) {
  bangun_datar bgn_dtr = new bangun_datar();
  persegi prs = new persegi(2);
  segitiga sgt = new segitiga(2, 3, 5, 2, 3);
  lingkaran ling = new lingkaran(2, 3.14);

  bgn_dtr.luas();
  bgn_dtr.keliling();

  print("Luas Persegi : ${prs.luas()}");
  print("Keliling Persegi : ${prs.keliling()}");
  print("Luas Segitiga : ${sgt.luas()}");
  print("Keliling Segitiga : ${sgt.keliling()}");
  print("Luas Lingkarab : ${ling.luas()}");
  print("Keliling Lingkaran : ${ling.keliling()}");
  


}