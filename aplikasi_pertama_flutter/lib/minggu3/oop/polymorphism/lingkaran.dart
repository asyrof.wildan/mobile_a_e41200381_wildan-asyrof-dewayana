import 'bangun_datar.dart';

class lingkaran extends bangun_datar{
  double r = 0;
  double pi = 3.14;

  lingkaran (double r, double pi){
    this.r = r;
    this.pi = 3.14;
  }

  @override
  double luas(){
    return pi * r *r;
  }

  @override
  double keliling(){
    return 2 * pi * r;
  }

}