import 'package:flutter/material.dart';
import 'package:materialapp/routes.dart';

void main() {
  runApp(MaterialApp(
    onGenerateRoute: RouteGenerator.generateRoute,
  ));
}