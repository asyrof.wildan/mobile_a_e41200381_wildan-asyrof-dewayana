import 'package:flutter/material.dart';
import 'package:mockup_widget_telegram/tugas11/Telegram.dart';

void main() => runApp(BelajarImage());

class BelajarImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // home: Telegram(),
      home: Scaffold(        
        appBar: AppBar(
          title: Text('BelajarFlutter.com')
        ),
        body: Image.asset('assets/images/tired.jpg'),
      )
    );
  }
}